# encoding: utf-8

'''
Reads the source configuration from app configuration,
retrieves the stations list for web resources (only),
downloads the stations data locally for web resources (only),
generates the stations list file for every source (so that the list is homogeneous from one source to another),
produces the png thumbnails.
All data is stored in files. The paths patterns are defined in utils/io_utils.py (class IoHelper). The root path can be
defined in the app's configuration

For now, all data is retrieved and re-generated at every run.
TODO: optimize the resource consumption.
- Only process data that have new values
- only get the new values for hydroweb resources available through api
'''

import argparse
import geojson
import json
import logging
import pygeoj
from pyproj import CRS, Transformer

logger = logging.getLogger()
SOURCE_CRS = 'epsg:4326'
DEST_CRS = 'epsg:4326'


# TODO : get CRS from geojson definition for later reprojection & manage krovak projection
# currently, we only support EPSG:4326 data so we have to transform it prior to running this script

def main():
    # Input arguments
    parser = argparse.ArgumentParser(description='''
    Reads the source configuration, scans the stations and produces:
     * the stations list in geojson format,
    similar to http://hydroweb.theia-land.fr/hydroweb/search?_m=light&lang=fr&basin=Niger&lake=&river=&status=&box=&q=
     * graph thumbnails for each station
    ''')
    parser.add_argument('-v', '--verbose', help='verbose output (debug loglevel)',
                        action='store_true')
    parser.add_argument('in_json_file', help='json input file, formatted like Presov City adresy file')
    parser.add_argument('out_sjson_file', help='sjson file like used by addok')
    parser.add_argument('--logfile',
                        help='logfile path. Default: prints logs to the console')
    args = parser.parse_args()


    # INITIALIZE LOGGER
    handler = logging.StreamHandler()
    if args.logfile:
        handler = logging.FileHandler(args.logfile)

    formatter = logging.Formatter(
            '%(asctime)s %(name)-5s %(levelname)-3s %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
    loglevel = logging.INFO
    if args.verbose:
        loglevel = logging.DEBUG
    logger.setLevel(loglevel)

    # will store the streets as a dict, formatted as expected by addok
    streets = dict()

    with open(args.in_json_file) as f:
        data = json.load(f)

        SOURCE_CRS = data['crs']['properties'].get('name')
        for feature in data['features']:
            properties = feature['properties']
            # get Point and add it as lat, lon in the properties
            geom = feature['geometry']['coordinates'][0]
            # Reproject to EPSG:4326
            #crs = CRS.from_wkt('PROJCS["S-JTSK_Krovak_East_North",GEOGCS["GCS_S_JTSK",DATUM["Jednotne_Trigonometricke_Site_Katastralni",SPHEROID["Bessel_1841",6377397.155,299.1528128]],PRIMEM["Greenwich",0],UNIT["Degree",0.017453292519943295]],PROJECTION["Krovak"],PARAMETER["False_Easting",0],PARAMETER["False_Northing",0],PARAMETER["Pseudo_Standard_Parallel_1",78.5],PARAMETER["Scale_Factor",0.9999],PARAMETER["Azimuth",30.28813975277778],PARAMETER["Longitude_Of_Center",24.83333333333333],PARAMETER["Latitude_Of_Center",49.5],PARAMETER["X_Scale",-1],PARAMETER["Y_Scale",1],PARAMETER["XY_Plane_Rotation",90],UNIT["Meter",1],AUTHORITY["EPSG","102067"]]')
            #print(crs.geodetic_crs)
            #proj = Transformer.from_crs(crs.geodetic_crs, CRS.from_string(DEST_CRS))
            #projected = proj.transform(geom[0], geom[1])
            properties['lon'] = geom[0]
            properties['lat'] = geom[1]
            _add_to_streets(properties, streets)

    # Write output file in Line-delimited JSON
    with open(args.out_sjson_file, 'w') as dst:
        for line in streets.values():
            dst.write(json.dumps(line))
            dst.write('\n')

    print(args.out_sjson_file)

def _add_to_streets(properties, streets):
    street = streets.get(properties['I_U'])
    if street is None:
        # create the street dict
        # TODO maybe extract the center of the street, or the first, instead of random point in the street
        street = {
            'type': 'street',
            'name': properties['N_U'],
            'city': 'Prešov',
            'region': 'Prešov',
            'district': 'Prešov',
            'importance': 0.5,
            'lat': properties['lat'],
            'lon': properties['lon'],
            'housenumbers': {}
        }
        pass
    else:
        #append the nb info
        pass
    if properties['CO']:
        street['housenumbers'][properties['CO']] = {
            'lat': properties['lat'],
            'lon': properties['lon']
        }
    streets[properties['I_U']] = street


if __name__ == '__main__':
    main()