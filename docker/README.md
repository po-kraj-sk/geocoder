# Run addok in docker

## Build 
    ```
    build . -t pigeosolutions/addok
    ```
   (adapt the image name if you want to)

## Run the composition:
Run on docker, locally (URL will be localhost)
You probably shouldn't be running it as-is on production. 
Please read the addok documentation for further configuration
```
docker-compose up -d
```
Check it works: open localhost:7878/search/?q=Pre , you should get an empty feature collection object. 
Then you should load some data into redis using the addok command:
 
## Load data into the redis database
* copy some data into your mounted folder
* load the data (ex with sample data included in the image)
```
docker-compose exec addok addok batch /sample_data/presov_city_adresy.sjson
docker-compose exec addok addok ngrams
```

## Remove old data from redis DB
```
docker-compose exec addok addok reset
```

Other actions are possible using the addok command. Please refer to addok documentation.
   
 